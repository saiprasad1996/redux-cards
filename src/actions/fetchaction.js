
export function fetchapicall(api){
    return function(dispatch){
        
        fetch(api)
        .then(response => response.text)
        .then((textresponse)=>{
            dispatch({type:"FETCH",payload:{api:api,response:textresponse}})
        })
        .catch(error=>{
            dispatch({type:"FETCH",payload:{api:api,response:error.toString()}})
        })

        
    }
}

export function clearState(){
    return function(dispatch){
        localStorage.clear()
        dispatch({type:"CLEAR"})
    }
}
