const initialState = {
    cards:[],
    fetches:[]
}

/*{name,age,company }
{id,api}

*/
const cardsReducer=(state=initialState,action)=>{
    
    const mstate = {...state}
    switch (action.type) {
        case "ADD_CARD":
            console.log("Add card")
            return {
                ...mstate,
                cards:mstate.cards.concat({id:Math.random(),name:action.payload.name,age:action.payload.age,company:action.payload.company})
            }
        case "REMOVE_CARD":
            return {
                ...mstate,
                cards:mstate.cards.filter(element => element.id !== action.payload.id)
            }
        case "FETCH":
            return{
                ...mstate,
                fetches:mstate.fetches.concat({id:Math.random(),api:action.payload.api,response:action.payload.response})
            }
        case "CLEAR":
            return {...initialState}
    
    }
    return mstate
}

export default cardsReducer;