import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {Provider } from 'react-redux';
import { createStore,applyMiddleware } from 'redux';

import reducer from './reducers/cardsreducer';
import fetchapicall  from "./actions/fetchaction";
import thunk from 'redux-thunk'



// function reducer(){
//     return 'State';
// }

// const store = createStore(reducer);

function saveToLocalStorage(state){
    try{
        const serializedState = JSON.stringify(state)
        localStorage.setItem('state',serializedState)

    }catch(e){
        console.log(e)
    }
}

function loadStoreFromLocalStorage(){
    try{
        const serializedState = localStorage.getItem('state')
        if (serializedState === null ) return undefined
        return JSON.parse(serializedState)
    }catch(e){
        console.log(e)
        return undefined
    }
}

const persistedState = loadStoreFromLocalStorage()


const store = createStore(reducer,persistedState,applyMiddleware(thunk));
store.subscribe(()=>saveToLocalStorage(store.getState()))

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
