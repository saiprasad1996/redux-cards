import React,{Component} from 'react';

import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { Input, Grid, Typography } from '@material-ui/core';

import {fetchapicall} from './actions/fetchaction';
import {clearState} from './actions/fetchaction';

class App extends Component{
    constructor(props){
        super(props)
        this.state={
            name:"",
            company:"",
            age:"",
            api:""
        }
    }
    // 'http://dummy.restapiexample.com/api/v1/employee/1'
    render(){
        return(
            <div>
                <Grid container>
                <Input type="text" onChange={(e)=>this.setState({name:e.target.value})} variant="bounded" placeholder="Name" />
                <br/>
                <Input type="number" onChange={(e)=>this.setState({age:e.target.value})} variant="bounded" placeholder="Age" />
                <br/>
                <Input type="text" onChange={(e)=>this.setState({company:e.target.value})} variant="bounded" placeholder="Company" />
                <br/>
                <Button variant="contained" color="primary" onClick={()=>this.props.addCard(this.state.name,this.state.company,this.state.age)}>Add Card</Button>
                
                <Input type="text" onChange={(e)=>this.setState({api:e.target.value})} variant="bounded" placeholder="Company" />
                <br/>
                <Button variant="contained" color="primary" onClick={()=>this.props.callApi(this.state.api)}>Call API</Button>
                <Button variant="contained" color="secondary" onClick={()=>this.props.clearState()}>Clear</Button>
                
                </Grid>
                <hr/>
                <Grid container spacing={2}>
                   { this.props.cards.map(val=>
                    <Grid item  xs={12} sm={3} md={3} lg={3} >
                    <Card>
                        
                        <CardContent>
                        <Typography>{val.name}</Typography>
                            <Typography color="textSecondary" gutterBottom>{val.company}</Typography>
                            <Typography color="textSecondary" gutterBottom>{val.age}</Typography>
                        </CardContent>
                        <CardActions>
                            <Button onClick={()=>this.props.removeCard(val.id)}>Delete</Button>
                        </CardActions>
                    </Card>
                </Grid>
                    )}
                </Grid>
                <hr/>
                <Grid container spacing={2}>
                   { this.props.fetches.map(val=>
                    <Grid item  xs={12} sm={3} md={3} lg={3} >
                    <Card>
                        
                        <CardContent>
                        <Typography>{val.id}</Typography>
                            <Typography color="textSecondary" gutterBottom>{val.api}</Typography>
                            <Typography color="textSecondary" gutterBottom>{val.response}</Typography>
                        </CardContent>
                        <CardActions>
                            {/* <Button onClick={()=>this.props.removeCard(val.id)}>Delete</Button> */}
                        </CardActions>
                    </Card>
                </Grid>
                    )}
                </Grid>
            </div>

        )
    }

    
}

const mapStateToProps=(store)=>{
    return{
        cards:store.cards,
        fetches:store.fetches
    }
}

const mapDispatchToprops = (dispatch) => {
    return{
        addCard : (name,company,age)=>dispatch({type:"ADD_CARD",payload:{name:name,company:company,age:age}}),
        removeCard : (id)=>dispatch({type:"REMOVE_CARD",payload:{id:id}}),
        callApi:(api)=>dispatch(fetchapicall(api)),
        clearState:()=>dispatch(clearState()),
    }
}


export default connect(mapStateToProps,mapDispatchToprops)(App);
