import React,{Component} from 'react';

import {connect} from 'react-redux';

class Counter extends Component{
   
    render(){
        return(
            <div>
                <div>
                    <h4>Counter : <span>{this.props.age}</span></h4>
                    <button onClick={this.props.onAgeUp}>Age Up</button>
                    <button onClick={this.props.onAgeDown}>Age down</button>
                </div>
            <hr/>
            <h2>History</h2>
            <div>
                <ul>
                    {
                        this.props.history.map((val,index)=>{
                                return (
                                    <li id={val.id} key={val.id} onClick={()=>this.props.onDeleteItem(val.id)}>
                                        {val.age}
                                    </li>
                                )
                        })
                    }
                </ul>
            </div>
            
            </div>
            
        )
    }

    
}

const mapStateToProps = (state)=>{
    return{
        age:state.age,
        history:state.history
    }
}

const mapDispatchToprops = (dispatch) => {
    return {
        onAgeUp:()=> dispatch({type:'AGE_UP',value:1}),
        onAgeDown:()=>dispatch({type:'AGE_DOWN',value:1}),
        onDeleteItem:(id)=>dispatch({type:"DELETE_ITEM",key:id})
    }
}

export default connect(mapStateToProps,mapDispatchToprops)(Counter);
